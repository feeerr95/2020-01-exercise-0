package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GildedRoseTest {

	public void advanceDays(int days, GildedRose app){
		for(; days > 0; days --){
			app.updateQuality();
		}
	}

	@Test
	public void theSellByDateHasPassed() {
		Item[] items = new Item[] { new Item("item1", 10, 20) };
		GildedRose app = new GildedRose(items);
		advanceDays(12, app);
		assertThat(6).isEqualTo(app.items[0].quality);
	}

	@Test
	public void agedBrieIncreasesQuality() {
		Item[] items = new Item[] { new Item("Aged Brie", 18, 10) };
		GildedRose app = new GildedRose(items);
		advanceDays(13, app);
		assertThat(28).isEqualTo(app.items[0].quality);
	}

	@Test
	public void sulfurasNeverDecreasesQuality() {
		Item[] items = new Item[] { new Item("Sulfuras", 10, 10) };
		GildedRose app = new GildedRose(items);
		advanceDays(60, app);
		assertThat(10).isEqualTo(app.items[0].quality);
	}

	@Test
	public void backstageDecreaseToZeroQuality() {
		Item[] items = new Item[] { new Item("Backstage passes", 18, 10) };
		GildedRose app = new GildedRose(items);
		advanceDays(19, app);
		assertThat(0).isEqualTo(app.items[0].quality);
	}

	@Test
	public void qualityCantBeNegative() {
		Item[] items = new Item[] { new Item("item 2", 10, 12) };
		GildedRose app = new GildedRose(items);
		advanceDays(19, app);
		assertThat(0).isEqualTo(app.items[0].quality);
	}

	@Test
	public void qualityCantBeMoreThanFifty() {
		Item[] items = new Item[] { new Item("Backstage passes 2nd", 20, 20) };
		GildedRose app = new GildedRose(items);
		advanceDays(19, app);
		assertThat(50).isEqualTo(app.items[0].quality);
	}

}
