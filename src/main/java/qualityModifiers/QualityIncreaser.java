package qualityModifiers;

import ar.uba.fi.tdd.exercise.Item;

public class QualityIncreaser extends QualityModifier{

    public QualityIncreaser(Item _item) {
        item = _item;
    }

    @Override
    public void updateQuality() {
        if(item.sellIn > 5 && item.sellIn <= 10)
            item.quality += 2;
        else if(item.sellIn > 0 && item.sellIn <= 5)
            item.quality += 3;
        else if(item.sellIn <= 0)
            item.quality = 0;
        else item.quality += 1;
        checkQuality();
        decreaseSellIn();
    }
}
