package qualityModifiers;

import ar.uba.fi.tdd.exercise.Item;

public class TwiceQualityDecreaser extends QualityModifier{

    public TwiceQualityDecreaser(Item _item) {
        item = _item;
    }
    @Override
    public void updateQuality() {
        item.quality -= 2;
        decreaseSellIn();
        checkQuality();
    }
}
