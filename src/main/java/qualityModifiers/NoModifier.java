package qualityModifiers;

import ar.uba.fi.tdd.exercise.Item;

public class NoModifier extends QualityModifier{

    public NoModifier(Item _item) {
        item = _item;
    }

    @Override
    public void updateQuality() {
        decreaseSellIn();
    }
}
