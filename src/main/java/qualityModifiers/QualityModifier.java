package qualityModifiers;

import ar.uba.fi.tdd.exercise.Item;
import exceptions.QualityCantBeMoreThanFiftyException;
import exceptions.QualityCantBeNegativeException;

public abstract class QualityModifier {

    protected Item item;

    public abstract void updateQuality();

    protected void decreaseSellIn(){
        item.sellIn -= 1;
    }

    public void checkQuality(){
        if(item.quality > 50) {
            item.quality = 50;
        }
        if(item.quality < 0) {
            item.quality = 0;
        }
    }
}
