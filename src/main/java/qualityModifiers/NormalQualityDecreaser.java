package qualityModifiers;

import ar.uba.fi.tdd.exercise.Item;

public class NormalQualityDecreaser extends QualityModifier{

    public NormalQualityDecreaser(Item _item) {
        item = _item;
    }

    @Override
    public void updateQuality() {
        item.quality -= 1;
        item.sellIn -= 1;
        checkQuality();
    }
}
