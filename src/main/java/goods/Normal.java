package goods;

import ar.uba.fi.tdd.exercise.Item;
import qualityModifiers.NormalQualityDecreaser;
import qualityModifiers.TwiceQualityDecreaser;

public class Normal extends Good{
    public Normal(Item _item) {
        item = _item;
        qualityModifier = new NormalQualityDecreaser(item);
    }

    @Override
    public void updateQuality() {
        if(item.sellIn <= 0)
            qualityModifier = new TwiceQualityDecreaser(item);
        qualityModifier.updateQuality();
    }
}
