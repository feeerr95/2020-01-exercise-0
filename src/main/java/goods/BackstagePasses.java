package goods;

import ar.uba.fi.tdd.exercise.Item;
import qualityModifiers.QualityIncreaser;

public class BackstagePasses extends Good{

    public BackstagePasses(Item _item) {
        item = _item;
        qualityModifier = new QualityIncreaser(item);
    }

    @Override
    public void updateQuality() {
        qualityModifier.updateQuality();
    }
}
