package goods;

import ar.uba.fi.tdd.exercise.Item;
import qualityModifiers.QualityIncreaser;

public class AgedBrie extends Good {

    public AgedBrie(Item _item) {
        item = _item;
        qualityModifier = new QualityIncreaser(item);
    }

    @Override
    public void updateQuality() {
        qualityModifier.updateQuality();
    }
}
