package goods;

import ar.uba.fi.tdd.exercise.Item;
import qualityModifiers.TwiceQualityDecreaser;

public class Conjured extends Good{

    public Conjured(Item _item) {
        item = _item;
        qualityModifier = new TwiceQualityDecreaser(item);
    }

    @Override
    public void updateQuality() {
        qualityModifier.updateQuality();
    }
}
