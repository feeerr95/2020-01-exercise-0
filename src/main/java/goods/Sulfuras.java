package goods;

import ar.uba.fi.tdd.exercise.Item;
import qualityModifiers.NoModifier;

public class Sulfuras extends Good{

    public Sulfuras(Item _item) {
        item = _item;
        qualityModifier = new NoModifier(item);
    }

    @Override
    public void updateQuality() {
        qualityModifier.updateQuality();
    }
}
