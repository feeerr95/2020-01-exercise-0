package goods;

import ar.uba.fi.tdd.exercise.Item;
import qualityModifiers.QualityModifier;

public abstract class Good {

    protected Item item;
    protected QualityModifier qualityModifier;
    public abstract void updateQuality();
}
