
package ar.uba.fi.tdd.exercise;

import goods.*;

class GildedRose {
    Item[] items;

    public GildedRose(Item[] _items) {
        items = _items;
    }

    private Good califacteItem (Item item){
        if(item.Name.contains("Aged Brie"))
            return new AgedBrie(item);
        if(item.Name.contains("Sulfuras"))
            return new Sulfuras(item);
        if(item.Name.contains("Backstage passes"))
            return new BackstagePasses(item);
        if(item.Name.contains("Conjured"))
            return new Conjured(item);
        return new Normal(item);
    }

    public void updateQuality() {
        for (int i = 0; i < items.length; i++) {
            califacteItem(items[i]).updateQuality();
        }
    };
}