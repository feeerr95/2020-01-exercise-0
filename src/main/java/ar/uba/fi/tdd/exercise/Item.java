package ar.uba.fi.tdd.exercise;

public class Item {

    public String Name;
    public int sellIn, quality;

    public Item(String _name, int _sellIn, int _quality) {
        Name = _name;
        sellIn = _sellIn;
        quality = _quality;
    }

    @Override
    public String toString() {
        return Name + ", " + sellIn + ", " + quality;
    }
}
